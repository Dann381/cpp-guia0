#ifndef AMINO_H
#define AMINO_H

class Amino{
    private:
        string nombre = "\0";
        int numero = 0;
        list<Atom> lista;
    public:
        /* contructores */
        Amino();
        Amino(string nombre, int numero);

        /* métodos get and set */
        void set_nombre(string nombre);
        void set_numero(int numero);
        void add_atom(Atom atom);
        void set_lista(list<Atom> lista);
        string get_nombre();
        int get_numero();
        list<Atom> get_lista();
};
#endif