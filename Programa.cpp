#include <list>
#include <iostream>
using namespace std;
#include "Coords.h"
#include "Atom.h"
#include "Amino.h"
#include "Cadena.h"
#include "Proteina.h"
#include "Menu.h"

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

int main(){
    Clear();
    Menu menu = Menu();
    list<Proteina> protein_list;
    list<Cadena> chain_list;
    list<Amino> amino_list;
    list<Atom> atom_list;
    list<Coords> coord_list;

    Coords c1 = Coords(0.1, 0.1, 0.1);
    Coords c2 = Coords(0.2, 0.2, 0.2);
    coord_list.push_back(c1);
    coord_list.push_back(c2);

    Atom atom1 = Atom("hidrogeno", 1);
    atom1.set_coords(c1);
    Atom atom2 = Atom("carbono", 6);
    atom2.set_coords(c2);
    atom_list.push_back(atom1);
    atom_list.push_back(atom2);


    list<Atom> lista1;
    lista1.push_back(atom1);
    lista1.push_back(atom2);
    list<Atom> lista2;
    lista2.push_back(atom1);
    lista2.push_back(atom2);



    Amino leucina = Amino("leucina", 1);
    Amino glicina = Amino("glicina", 2);
    Amino arginina = Amino("arginina", 3);
    Amino treonina = Amino("treonina", 4);
    amino_list.push_back(leucina);
    amino_list.push_back(glicina);
    amino_list.push_back(arginina);
    amino_list.push_back(treonina);

    leucina.set_lista(lista1);
    glicina.set_lista(lista2);
    arginina.set_lista(lista1);
    treonina.set_lista(lista2);

    list<Amino> listaamino1;
    listaamino1.push_back(leucina);
    listaamino1.push_back(glicina);
    list<Amino> listaamino2;
    listaamino2.push_back(arginina);
    listaamino2.push_back(treonina);

    Cadena cadena1 = Cadena("A");
    cadena1.set_lista(listaamino1);

    Cadena cadena2 = Cadena("B");
    cadena2.set_lista(listaamino2);
    chain_list.push_back(cadena1);
    chain_list.push_back(cadena2);

    Proteina prot = Proteina("0000", "Miosina");
    list<Cadena> cadenas;
    cadenas.push_back(cadena1);
    cadenas.push_back(cadena2);
    
    prot.set_lista(cadenas);
    protein_list.push_back(prot);
    

    int opt = 0;
        while (opt != 3)
    {
        opt = menu.opciones();
       if (opt == 1)
       {
           Clear();
           for (Proteina proteinas : protein_list) {
               cout << "ID: " + proteinas.get_id();
               cout << " || Nombre: " + proteinas.get_nombre();
               cout << endl;
            }
       }
       else if (opt == 2){
            Clear();
            int opcion;
            opcion = menu.add_datos();
            if (opcion == 1)
            {
                Clear();
                string id;
                string nombre;
                cout << "Ingrese id y nombre de la proteína" << endl;
                cout << "ID: ";
                cin >> id;
                cout << "Nombre: ";
                cin >> nombre;
                Proteina nuevo = Proteina(id, nombre);
                protein_list.push_back(nuevo);
                Clear();
                cout << "Proteína agregada" << endl;
           }
           else if (opcion == 2)
           {
               Clear();
               string letra;
               cout << "Ingrese letra de la cadena" << endl;
               cout << "Letra: ";
               cin >> letra;
               Cadena cadena = Cadena(letra);
               chain_list.push_back(cadena);
               Clear();
               cout << "Cadena agregada" << endl;
           }
           else if (opcion == 3)
           {
               Clear();
               string nombre;
               int numero;
               cout << "Ingrese nombre y número del aminoácido" << endl;
               cout << "Nombre: ";
               cin >> nombre;
               cout << "Número: ";
               cin >> numero;
               Amino amino = Amino(nombre, numero);
               amino_list.push_back(amino);
               Clear();
               cout << "Aminoácido agregado" << endl;
           }
           else if (opcion == 4)
           {
               Clear();
               string nombre;
               int numero;
               cout << "Ingrese nombre y número del átomo" << endl;
               cout << "Nombre: ";
               cin >> nombre;
               cout << "Número: ";
               cin >> numero;
               Atom atomo = Atom(nombre, numero);
               atom_list.push_back(atomo);
               Clear();
               cout << "Aminoácido agregado" << endl;
           }
           else if (opcion == 5)
           {
               Clear();
               int x, y, z;
               cout << "Ingrese coordenadas (x, y, z)" << endl;
               cout << "X: ";
               cin >> x;
               cout << "Y: ";
               cin >> y;
               cout << "Z: ";
               cin >> z;
               Coords coords = Coords(x, y, z);
               coord_list.push_back(coords);
               Clear();
               cout << "Coordenadas agregadas" << endl;
           }
           else
           {
               Clear();
               cout << "Ingrese un número válido" << endl;
           }
           
       }
       else if (opt == 3)
       {
           Clear();
           cout << "Saliendo" << endl;
       }
       else
       {
           Clear();
           cout << "Ingrese un número válido" << endl;
       }
       cout << endl;        
    }
    return 0;
}