#include <list>
#include <iostream>
using namespace std;
#include "Coords.h"
#include "Atom.h"
#include "Amino.h"

/* constructores */
Amino::Amino()
{

    string nombre = "\0";
    int numero = 0;
    list<Atom> lista;
}

Amino::Amino(string nombre, int numero)
{
    this->nombre = nombre;
    this->numero = numero;

}
/* métodos get and set */

void Amino::set_nombre(string nombre)
{
    this->nombre = nombre;
}

void Amino::set_numero(int numero)
{
    this->numero = numero;
}

void Amino::add_atom(Atom atomo)
{
    this->lista.push_back(atomo);
}

void Amino::set_lista(list<Atom> lista)
{
    this->lista = lista;
}

string Amino::get_nombre()
{
    return this->nombre;
}

int Amino::get_numero()
{
    return this->numero;
}

list<Atom> Amino::get_lista()
{
    return this->lista;
}