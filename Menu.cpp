#include <iostream>
using namespace std;
#include "Menu.h"


/* constructores */
Menu::Menu(){}

/* métodos get and set */

int Menu::opciones()
{
    cout << "Elija una opción" << endl;
    cout << "1.- Ver lista de proteínas" << endl;
    cout << "2.- Agregar datos" << endl;
    cout << "3.- Salir" << endl;
    cout << "Opción: ";
    int opt;
    cin >> opt;
    return opt;
}

int Menu::add_datos()
{
    cout << "¿Qué desea agregar?" << endl;
    cout << "1.- Proteína" << endl;
    cout << "2.- Cadena" << endl;
    cout << "3.- Aminoácido" << endl;
    cout << "4.- Átomo" << endl;
    cout << "5.- Coordenada" << endl;
    cout << "Opción: ";
    int opt;
    cin >> opt;
    return opt;
}
