#ifndef COORDS_H
#define COORDS_H

class Coords{
    private:
        float x = 0.0;
        float y = 0.0;
        float z = 0.0;
    public:
        /* contructores */
        Coords();
        Coords(float x, float y, float z);

        /* métodos get and set */
        void set_x(float x);
        void set_y(float y);
        void set_z(float z);
        float get_x();
        float get_y();
        float get_z();
};
#endif