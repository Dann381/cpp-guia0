# Guía 0

La presente guía tiene la finalidad de simular una pequeña "base de datos" de proteínas, la cual es manipulada mediante un menú que se muestra a través de la terminal, y que permite agregar datos, leer las proteínas agregadas, o terminar el programa.

### Ejecución
El archivo que actúa como main es "Programa.cpp", el cuál debe ejecutarse para poder iniciar el programa. Luego de ejecutar el archivo, se mostrará un menú mediante la terminal por la cual el usuario podrá elegir entre las opciones que posee el programa.
Al terminar, todos los datos agregados se borrarán, pues aún no se ha implementado un método para guardar los datos en algún archivo, ya que este programa solo está en una fase inicial, pero contiene todo lo pedido en el archivo pdf adjunto en este repositorio llamado "guia0-U1".

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
