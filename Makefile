prefix=/usr/local
CC = g++
CFLAGS = -g -Wall 
SRC = Programa.cpp Proteina.cpp Cadena.cpp Amino.cpp Atom.cpp Coords.cpp Menu.cpp
OBJ = Programa.o Proteina.o Cadena.o Amino.o Atom.o Coords.o Menu.o
APP = programa
all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 
clean:
	$(RM) $(OBJ) $(APP)
install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin
uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
