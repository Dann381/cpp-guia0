#include <iostream>
using namespace std;
#include "Coords.h"
#include "Atom.h"

/* constructores */
Atom::Atom()
{

    string nombre = "\0";
    int numero = 0;
    Coords coords;
}

Atom::Atom(string nombre, int numero)
{
    this->nombre = nombre;
    this->numero = numero;

}
/* métodos get and set */

void Atom::set_nombre(string nombre)
{
    this->nombre = nombre;
}

void Atom::set_numero(int numero)
{
    this->numero = numero;
}

void Atom::set_coords(Coords coords)
{
    this->coords = coords;
}

string Atom::get_nombre()
{
    return this->nombre;
}

int Atom::get_numero()
{
    return this->numero;
}

Coords Atom::get_coords()
{
    return this->coords;
}