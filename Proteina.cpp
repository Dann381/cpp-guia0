#include <list>
#include <iostream>
using namespace std;
#include "Coords.h"
#include "Atom.h"
#include "Amino.h"
#include "Cadena.h"
#include "Proteina.h"

/* constructores */
Proteina::Proteina()
{
    string id = "\0";
    string nombre = "\0";
    list<Cadena> lista;
}

Proteina::Proteina(string id, string nombre)
{
    this->id = id;
    this->nombre = nombre;

}
/* métodos get and set */

void Proteina::set_id(string id)
{
    this->id = id;
}

void Proteina::set_nombre(string nombre)
{
    this->nombre = nombre;
}

void Proteina::add_cadena(Cadena cadena)
{
    this->lista.push_back(cadena);
}

void Proteina::set_lista(list<Cadena> lista)
{
    this->lista = lista;
}

string Proteina::get_id()
{
    return this->id;
}

string Proteina::get_nombre()
{
    return this->nombre;
}

list<Cadena> Proteina::get_lista()
{
    return this->lista;
}