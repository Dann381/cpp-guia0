#include <iostream>
using namespace std;
#include "Coords.h"

/* constructores */
Coords::Coords()
{
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;
}

Coords::Coords(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

/* métodos get and set */
void Coords::set_x(float x)
{
    this->x = x;
}

void Coords::set_y(float y)
{
    this->y = y;
}

void Coords::set_z(float z)
{
    this->z = z;
}

float Coords::get_x()
{
    return this->x;
}

float Coords::get_y()
{
    return this->y;
}

float Coords::get_z()
{
    return this->z;
}