#ifndef ATOM_H
#define ATOM_H

class Atom{
    private:
        string nombre = "\0";
        int numero = 0;
        Coords coords;
    public:
        /* contructores */
        Atom();
        Atom(string nombre, int numero);

        /* métodos get and set */
        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_coords(Coords coords);
        string get_nombre();
        int get_numero();
        Coords get_coords();
};
#endif