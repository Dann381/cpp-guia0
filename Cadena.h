#ifndef CADENA_H
#define CADENA_H

class Cadena{
    private:
        string letra = "\0";
        list<Amino> lista;
    public:
        /* contructores */
        Cadena();
        Cadena(string letra);

        /* métodos get and set */
        void set_letra(string letra);
        void add_amino(Amino amino);
        void set_lista(list<Amino> lista);
        string get_letra();
        list<Amino> get_lista();
};
#endif