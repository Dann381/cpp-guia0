#ifndef MENU_H
#define MENU_H

class Menu{
    public:
        /* contructores */
        Menu();

        /* métodos get and set */
        int opciones();
        int add_datos();
};
#endif