#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina{
    private:
        string id = "\0";
        string nombre = "\0";
        list<Cadena> lista;

    public:
        /* contructores */
        Proteina();
        Proteina(string id, string nombre);

        /* métodos get and set */
        void set_id(string id);
        void set_nombre(string nombre);
        void add_cadena(Cadena cadena);
        void set_lista(list<Cadena> lista);
        string get_id();
        string get_nombre();
        list<Cadena> get_lista();
};
#endif